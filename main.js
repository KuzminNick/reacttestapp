import React from 'react';
import ReactDOM from 'react-dom';
import Clock from './ClockComponent.jsx';

let counter = 0;
function tick() {
	const element = <Clock counter={counter} />;
	ReactDOM.render(element, document.getElementById('app'));
	counter++;
}

setInterval(tick, 1000);
