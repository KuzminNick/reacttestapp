import React from 'react';

class Clock extends React.Component {
	render() {
		let className = 'time-container';
		if (this.props.counter % 2 == 0) {
			className += ' grey';
		}
	
		return (
			<div className={className}>
				<h1>Hello, world!</h1>
				<h2>It is {new Date().toLocaleTimeString()}.</h2>
			</div>);
	}
}

export default Clock;