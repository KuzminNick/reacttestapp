import React from 'react';
import PropTypes from 'prop-types';

export function InfoBlockComponent({ color, handleComponentClick }) {
  return (
    <div>
      <h1 id='info-block-header' style={{ color }}>
        I am a {color} component
      </h1>
      <button
        id='info-block-button'
        type='button'
        onClick={() => handleComponentClick({ startTime: 223 })} >
        Click
      </button>
    </div>
  );
}
InfoBlockComponent.propTypes = {
  color: PropTypes.string.isRequired,
  handleComponentClick: PropTypes.func.isRequired
};
export default InfoBlockComponent;
