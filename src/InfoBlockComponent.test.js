import React from 'react';
import { mount, shallow } from 'enzyme';
import { InfoBlockComponent } from './InfoBlockComponent';
import toJson from 'enzyme-to-json';

describe('<InfoBlockComponent />', () => {
  let wrapper;
  let handleComponentClick;
  beforeAll(() => {
    handleComponentClick = jest.fn();
    wrapper = shallow(<InfoBlockComponent color='red' handleComponentClick={handleComponentClick} />);
  });
  
  it('matches snapshot', () => {
    expect(toJson(wrapper)).toMatchSnapshot();
  });
  
  // check dynamic data renders correctly
  // find elements by ID
  it('renders the heading', () => {
    const heading = wrapper.find('#info-block-header');
    expect(heading).toHaveLength(1);
    expect(heading.text()).toEqual('I am a red component');
  });
  
  // check actions
  it('calls handleStartCar on button click', function () {
    const button = wrapper.find('#info-block-button');
    expect(button).toHaveLength(1);
    button.simulate('click');
    expect(handleComponentClick).toHaveBeenCalledWith({ startTime: 223 });
  });
});
